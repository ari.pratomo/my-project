const firstCapital = (text) => text.charAt(0).toUpperCase() + text.slice(1)

module.exports = function (plop) {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
        filter: firstCapital,
      },
    ],
    actions: [
      // {
      //   type: 'add',
      //   path: 'src/components/{{name}}/Styled.js', // styled-component with styled-system
      //   templateFile: 'templates/Styled.js.hbs',
      // },
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.svelte', // component
        templateFile: 'templates/Component.svelte.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.test.js', // component unit test
        templateFile: 'templates/Component.test.js.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__stories__/Data.js', // component's dummy data
        templateFile: 'templates/Data.js.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__stories__/{{name}}.stories.js', // component stories in csf format
        templateFile: 'templates/Component.stories.js.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__stories__/{{name}}.doc.mdx', // component stories in mdx format
        templateFile: 'templates/Component.doc.mdx.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__stories__/{{name}}.interaction.mdx', // component interaction documentation in mdx format
        templateFile: 'templates/Component.interaction.mdx.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__stories__/{{name}}.code.mdx', // component code documentation in mdx format
        templateFile: 'templates/Component.code.mdx.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.overview.stories.mdx', // component stories overview in mdx format
        templateFile: 'templates/Component.overview.stories.mdx.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/index.js', // component index for monolith export
        templateFile: 'templates/index.js.hbs',
      },
    ],
  })
}
