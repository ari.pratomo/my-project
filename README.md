# SERUNI

UI Kit for webview. Built using [sveltejs](https://svelte.dev), [storybook](https://storybook.js.org/), & [Testing-Library](https://testing-library.com/)

## Setup

1. Install nodejs & npm minimum version: `nodejs v12.14.0 lts` & `npm 6.14.8`
2. clone repository: `git clone gitlab.linkaja.com/fe/seruni seruni`
3. install dependency package: `npm i`
4. run the storybook: `npm run sb`
5. after storybook running, run the creevey for visual-regression-test:
   `npm run cui` (make sure you have docker installed)
6. run build for storybook: `npm run bsb`
7. run `npm run build` if you want all the components could be imported by another project

## Standard

1. Create component under `components` folder
2. Each component must be created on separate folder & contain `index.js` for exporting purpose
3. Add component `index.js` path to `rollup.config.js` on `const compnentList` to export the component definition
4. Stories must cover all cases
5. Add [control](https://storybook.js.org/docs/react/essentials/controls) to stories
6. Add documentation, better on [mdx](https://storybook.js.org/docs/react/writing-docs/mdx)
7. The component must be unit-tested with coverage 100% for `branches`, `lines`, `statements` & `functions`

❗ This project using `husky` git hooks to run formatter, linter & unit test coverage checker before commit. if one of those failed, your code will not be commited as well.

## Test

### Unit Test

Tools: Jest, Testing-Libary

How to:

- `npm run test`
- `npm run test-cov`

### Visual Regression Test

Tools: Creevey

How to:

1. Before you change a component or create new component, make sure creevey update snapshot for reference to the new changes.
2. Run `npm run cui` to start creevey ui.
3. Open creevey service on browser, by default on localhost:3000. Run the checking process. If there is any failed check related to snapshoot reference, you can update all snapshoot by execute `npm run cup` on terminal.

Now base reference has been set, you can make any change or adding new component.
After you finish your work, you can check whether you make unwanted changes by rerun creevey check on browser.

## Usage

This is how to import seruni on your project

- If you already clone this repo on your local machine, for development purpose, you can import using `npm i -D file:../seruni`
- If you want to use on origin version, you can import using `npm i -D git+https://ari:taUzrGja3yetuwSydQ8Z@gitlab.linkaja.com/fe/seruni.git#master`
  Then you can import on your page. For example:
  ```javascript
   import { Button } from "seruni";
   ...
   <Button data={buttonData} />
  ```
  OR
  ```javascript
   import Button from "seruni/components/button";
   ...
   <Button data={buttonData} />
  ```

## Resources

- [Testing](https://tcashsquad.atlassian.net/wiki/spaces/DEVOPS/pages/1046773881/Testing)
- [Svelte-Testing-Library](https://testing-library.com/docs/svelte-testing-library/intro/)
- [Svelte-Storybook](https://www.learnstorybook.com/intro-to-storybook/svelte/en/get-started/)
- [Code-Splitting](https://levelup.gitconnected.com/code-splitting-for-libraries-bundling-for-npm-with-rollup-1-0-2522c7437697)
- [Selenium](https://www.selenium.dev/selenium/docs/api/javascript/) (used to make testing script on creevey)
