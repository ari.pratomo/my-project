const path = require("path");

module.exports = {
  webpackFinal: async (config) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      "~": path.resolve(__dirname),
      "@": path.resolve(__dirname, "../src/"),
      "@components": path.resolve(__dirname, '../src/components'),
      "@helpers": path.resolve(__dirname, '../src/helpers'),
      "@stores": path.resolve(__dirname, '../src/stores'),
      "@constants": path.resolve(__dirname, '../src/constants'),
      "@utils": path.resolve(__dirname, '../src/utils'),
    };
    config.resolve.extensions.push(".ts", ".tsx", ".js", ".jsx", ".svelte", ".json");
    return config;
  },
  stories: [
    "../src/**/*.overview.stories.mdx", 
    "../src/**/*.stories.js",
  ],
  addons: [
    "@storybook/addon-docs",
    "@storybook/addon-controls",
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "creevey"
  ]
}