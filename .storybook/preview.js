import '../public/global.css';
import '../public/fontello.css';
import '../public/animation.css';
import React from 'react'
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
// import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';

export const decorators = [(Story) => <div><Story /></div>];

export const parameters = {
  creevey: {
    captureElement: null,
    skip: [
      { stories: /.*Data$/ }
    ],
  },
  // docs: {
  //   container: DocsContainer,
  //   page: DocsPage,
  // },
  viewport: {
    viewports: INITIAL_VIEWPORTS,
    // defaultViewport: 'iphone5',
  },
  backgrounds: {
    default: 'light',
    values: [
      {
        name: 'light',
        value: '#ffffff',
      },
      {
        name: 'dark',
        value: '#aaaaaa',
      },
    ],
  },
  actions: { argTypesRegex: "^on[A-Z].*" }
}
