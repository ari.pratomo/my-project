module.exports = {
  transform: {
    "^.+\\.[tj]sx?$": "babel-jest",
    "^.+\\.mdx$": "@storybook/addon-docs/jest-transform-mdx",
    "^.+\\.stories\\.[jt]sx?$":
      "<rootDir>node_modules/@storybook/addon-storyshots/injectFileName",
    "^.+\\.svelte$": [
      "svelte-jester",
      {
        debug: false,
      },
    ],
  },
  bail: false,
  verbose: true,
  moduleFileExtensions: ["js", "svelte", "json"],
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/__mocks__/fileMock.js",
    "\\.(css|scss|stylesheet)$": "<rootDir>/__mocks__/styleMock.js",
    "@constants/(.*)": "<rootDir>/src/constants/$1",
    "@components/(.*)": "<rootDir>/src/components/$1",
    "@stores/(.*)": "<rootDir>/src/stores/$1",
    '@utils/(.*)': '<rootDir>/src/utils/$1',
  },
  setupFilesAfterEnv: ["@testing-library/jest-dom/extend-expect"],
  testPathIgnorePatterns: ["/node_modules/", "/build/", "/storybook-static/"],
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "/build/",
    "/storybook-static/",
    "/.storybook/",
  ],
  coverageProvider: "babel",
  collectCoverageFrom: ["src/components/**/*.svelte", "src/stores/**/*.js"],
  coverageThreshold: {
    "./src/components/": {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
};
