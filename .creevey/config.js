const path = require('path');

module.exports = {
  // Default Storybook url
  storybookUrl: 'http://localhost:6006',

  // Where original images are stored
  screenDir: path.join(__dirname, 'images'),

  // Report directory that contains data from previous runs
  reportDir: path.join(__dirname, 'report'),

  // Pixelmatch options
  diffOptions: { threshold: 0.1 },

  // How many times test should be retried before to consider it as failed
  maxRetries: 2,

  // Describe browsers and their options
  browsers: {
    // Shorthand declarations of browsers
    chrome: true,
    'mobile-chrome': {
      browserName: 'chrome',
      // Define initial viewport size
      viewport: { width: 320, height: 568 },
      // Increase parallel sessions
      limit: 2,
      /* Also you can define any browser capabilities here */
      // version: '86.0',
    },
  }
};