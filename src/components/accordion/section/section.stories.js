import Accordion from "./section";
import SimpleMultipleComp from "./MockStories/simple-multiple";
import { CompGroup } from "@constants/story";

export default {
  title: `${CompGroup}/Accordion`,
  component: Accordion,
  excludeStories: /.*Data$/,
  argTypes: {
    key: {
      description: "Key of accordion, must be unique on each",
      control: "text",
      type: {
        summary: "string",
      },
    },
  },
};

const Template = ({ ...args }) => ({
  Component: Accordion,
  props: { ...args },
  on: {},
});

export const Default = Template.bind({});
Default.args = {
  key: "1",
};
Default.parameters = {
  creevey: {
    captureElement: "#root",
    tests: {
      async click() {
        const start = await this.takeScreenshot();

        const accordion = await this.browser.findElement({
          css: "#accordion-header-tid-1",
        });
        await this.browser
          .actions()
          .click(accordion)
          .perform();
        const open = await this.takeScreenshot();

        await this.browser
          .actions()
          .click(accordion)
          .perform();
        const close = await this.takeScreenshot();

        await this.expect({ start, open, close }).to.matchImages();
      },
    },
  },
};

export const SimpleMultiple = () => ({
  Component: SimpleMultipleComp,
  props: {},
  on: {},
});
SimpleMultiple.parameters = {
  creevey: {
    captureElement: "#root",
    tests: {
      async click() {
        const startm = await this.takeScreenshot();

        const accordion1 = await this.browser.findElement({
          css: "#accordion-header-tid-2",
        });
        await this.browser
          .actions()
          .click(accordion1)
          .perform();
        const clicked1 = await this.takeScreenshot();

        const accordion2 = await this.browser.findElement({
          css: "#accordion-header-tid-3",
        });
        await this.browser
          .actions()
          .click(accordion2)
          .perform();
        const clicked2 = await this.takeScreenshot();

        await this.expect({ startm, clicked1, clicked2 }).to.matchImages();
      },
    },
  },
};
