import {
  render,
  fireEvent,
  waitForElementToBeRemoved,
  cleanup,
  screen,
} from "@testing-library/svelte";
import Accordion from "./section";

describe("accordion", () => {
  afterEach(() => {
    cleanup();
  });

  it("single accordion", async () => {
    expect.hasAssertions();
    const key = "1";
    const testHeaderId = `accordion-header-tid-${key}`;
    const testContentId = `accordion-content-tid-${key}`;

    const { getByTestId, queryByTestId, container } = render(
      Accordion,
      { key: key },
      {}
    );

    await screen.findByTestId(testHeaderId);
    expect(getByTestId(testHeaderId)).toBeInTheDocument();
    expect(queryByTestId(testContentId)).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();

    fireEvent.click(getByTestId(testHeaderId));
    await screen.findByTestId(testContentId);
    expect(getByTestId(testContentId)).toBeInTheDocument();
    expect(container).toMatchSnapshot();

    fireEvent.click(getByTestId(testHeaderId));
    await waitForElementToBeRemoved(() => getByTestId(testContentId));
    expect(queryByTestId(testContentId)).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();

    cleanup();
  });

  it("multiple accordion", async () => {
    expect.hasAssertions();
    const key1 = "2";
    const testHeaderId1 = `accordion-header-tid-${key1}`;
    const testContentId1 = `accordion-content-tid-${key1}`;
    const key2 = "3";
    const testHeaderId2 = `accordion-header-tid-${key2}`;
    const testContentId2 = `accordion-content-tid-${key2}`;

    const { getByTestId, queryByTestId } = render(Accordion, { key: key1 }, {});
    const { container } = render(Accordion, { key: key2 }, {});

    // default
    await screen.findByTestId(testHeaderId1);
    await screen.findByTestId(testHeaderId2);
    expect(getByTestId(testHeaderId1)).toBeInTheDocument();
    expect(queryByTestId(testContentId1)).not.toBeInTheDocument();
    expect(getByTestId(testHeaderId2)).toBeInTheDocument();
    expect(queryByTestId(testContentId2)).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();

    // click on first accordion
    fireEvent.click(getByTestId(testHeaderId1));
    await screen.findByTestId(testContentId1);
    expect(getByTestId(testHeaderId1)).toBeInTheDocument();
    expect(getByTestId(testContentId1)).toBeInTheDocument();
    expect(getByTestId(testHeaderId2)).toBeInTheDocument();
    expect(queryByTestId(testContentId2)).not.toBeInTheDocument();
    expect(container).toMatchSnapshot();

    //click on second accordion
    fireEvent.click(getByTestId(testHeaderId2));
    await screen.findByTestId(testContentId2);
    await waitForElementToBeRemoved(() => getByTestId(testContentId1));
    expect(getByTestId(testHeaderId1)).toBeInTheDocument();
    expect(queryByTestId(testContentId1)).not.toBeInTheDocument();
    expect(getByTestId(testHeaderId2)).toBeInTheDocument();
    expect(getByTestId(testContentId2)).toBeInTheDocument();
    expect(container).toMatchSnapshot();

    cleanup();
  });
});
