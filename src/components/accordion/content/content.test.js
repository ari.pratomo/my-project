import { render, waitFor } from "@testing-library/svelte";
import Content from "./content";

const key = "1";
const testContentId = `accordion-content-tid-${key}`;

describe("accordion-Content", () => {
  it("open", async () => {
    expect.hasAssertions();
    const { getByTestId } = render(Content, { open: true, key: key }, {});
    let content;
    await waitFor(() => {
      content = getByTestId(testContentId);
      expect(content).toBeInTheDocument();
      expect(content).toMatchSnapshot();
    });
  });

  it("close", async () => {
    expect.hasAssertions();
    const { queryByTestId } = render(Content, { open: false, key: key }, {});
    let content;
    await waitFor(() => {
      content = queryByTestId(testContentId);
      expect(content).toBeNull();
      expect(content).toMatchSnapshot();
    });
  });
});
