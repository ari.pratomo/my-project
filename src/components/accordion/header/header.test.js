import { render, waitFor } from "@testing-library/svelte";
import Header from "./header";

const key = "1";
const testHeaderId = `accordion-header-tid-${key}`;
const testHeaderArrowId = `accordion-header-arrow-tid-${key}`;

describe("accordion-Header", () => {
  it("open", async () => {
    expect.hasAssertions();
    const { getByTestId } = render(Header, { open: true, key: key }, {});
    let header;
    let arrow;
    await waitFor(() => {
      header = getByTestId(testHeaderId);
      arrow = getByTestId(testHeaderArrowId);

      expect(header.contains(arrow)).toBe(true);
      expect(arrow.classList.contains("arrowUp")).toBe(true);
      expect(arrow.classList.contains("arrowDown")).toBe(false);
      expect(header).toMatchSnapshot();
    });
  });

  it("close", async () => {
    expect.hasAssertions();
    const { getByTestId } = render(Header, { open: false, key: key }, {});
    let header;
    let arrow;
    await waitFor(() => {
      header = getByTestId(testHeaderId);
      arrow = getByTestId(testHeaderArrowId);

      expect(header.contains(arrow)).toBe(true);
      expect(arrow.classList.contains("arrowUp")).toBe(false);
      expect(arrow.classList.contains("arrowDown")).toBe(true);
      expect(header).toMatchSnapshot();
    });
  });
});
