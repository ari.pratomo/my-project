import { render, fireEvent, waitFor } from "@testing-library/svelte";
import { type } from "@constants/button";
import { buttonData } from "@components/button/pure/data";
import Button from "./bottom";

const bottomTestId = "button-bottom-tid";
const testId = "button-tid";

describe("button-Bottom", () => {
  it("default", async () => {
    expect.hasAssertions();

    const mockClick = jest.fn();
    const { getByTestId, container, component } = render(Button, {
      props: { ...buttonData, title: "Button" },
    });
    let buttonBottom;
    let button;
    await waitFor(() => {
      buttonBottom = getByTestId(bottomTestId);
      button = getByTestId(testId);
      expect(buttonBottom.classList.contains("bottom")).toBe(true);
      expect(button.classList.contains(type.primary)).toBe(true);
      expect(container).toMatchSnapshot();

      component.$on("click", mockClick);
      fireEvent.click(button);
    });
    expect(mockClick).toHaveBeenCalledTimes(1);
  });
});
