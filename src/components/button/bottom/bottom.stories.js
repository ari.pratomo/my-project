import { argData, buttonData } from "../pure/data";
import Button from "./bottom";
import { CompGroup } from "@constants/story";

export default {
  title: `${CompGroup}/Button/Bottom`,
  component: Button,
  excludeStories: /.*Data$/,
  argTypes: argData,
};

const Template = ({ onClick, ...args }) => ({
  Component: Button,
  props: {
    ...args,
  },
  on: {
    click: onClick,
  },
});

export const Default = Template.bind({});
Default.args = {
  ...buttonData,
  title: "Button",
};
