export const buttonData = {
  title: null,
  type: null,
  disabled: false,
  loading: false,
};

export const argData = {
  title: {
    description: "What button text to be shown",
    control: "text",
    type: {
      summary: "string",
    },
  },
  type: {
    description: "Button type style",
    control: {
      type: "select",
      options: ["primary", "secondary", "transparent"],
    },
    type: {
      summary: "string",
    },
    defaultValue: {
      summary: "primary",
      detail: "primary | secondary | transparent",
    },
  },
  disabled: {
    description: "Disable state of button, can not be clicked",
    type: {
      summary: "bool",
    },
    control: { type: "select", options: [true, false] },
    defaultValue: {
      summary: "false",
      detail: "true | false",
    },
  },
  loading: {
    description: "Loading state of button, can not be clicked",
    type: {
      summary: "bool",
    },
    control: { type: "select", options: [true, false] },
    defaultValue: {
      summary: "false",
      detail: "true | false",
    },
  },
  onClick: {
    description: "What action to be perform when button clicked",
    action: "onClick",
    type: {
      summary: "func",
    },
  },
};
