import { render, fireEvent, waitFor } from "@testing-library/svelte";
import { type } from "@constants/button";
import { buttonData } from "./data";
import Button from "./pure";

const testId = "button-tid";

describe("button", () => {
  it("default", async () => {
    expect.hasAssertions();

    const mockClick = jest.fn();
    const { getByTestId, component } = render(Button, { props: buttonData });
    let button;
    await waitFor(() => {
      button = getByTestId(testId);
      expect(button).toBeEmptyDOMElement();
      expect(button.classList.contains(type.primary)).toBe(true);
      expect(button).toMatchSnapshot();

      component.$on("click", mockClick);
      fireEvent.click(button);
    });
    expect(mockClick).toHaveBeenCalledTimes(1);
  });

  it("loading", async () => {
    expect.hasAssertions();

    const mockClick = jest.fn();
    const { getByTestId, component } = render(Button, {
      props: { ...buttonData, loading: true },
    });
    let button;
    await waitFor(() => {
      button = getByTestId(testId);
      expect(button.firstChild.children[0].classList.contains("spinner")).toBe(
        true
      );
      expect(button).toMatchSnapshot();

      component.$on("click", mockClick);
      fireEvent.click(button);
    });
    expect(mockClick).toHaveBeenCalledTimes(0);
  });

  it("disabled", async () => {
    expect.hasAssertions();

    const mockClick = jest.fn();
    const { getByTestId, component } = render(Button, {
      props: { ...buttonData, disabled: true },
    });
    let button;
    await waitFor(() => {
      button = getByTestId(testId);
      expect(button).toHaveAttribute("disabled");
      expect(button).toMatchSnapshot();

      component.$on("click", mockClick);
      fireEvent.click(button);
    });
    expect(mockClick).toHaveBeenCalledTimes(0);
  });
  it("undefined", async () => {
    expect.hasAssertions();

    const mockClick = jest.fn();
    const { getByTestId, component } = render(Button);
    await waitFor(() => {
      const button = getByTestId(testId);
      expect(button).toBeEmptyDOMElement();
      expect(button.classList.contains("button")).toBe(true);
      expect(button).toMatchSnapshot();

      component.$on("click", mockClick);
      fireEvent.click(button);
    });
    expect(mockClick).toHaveBeenCalledTimes(1);
  });

  it("title", async () => {
    expect.hasAssertions();

    const { getByTestId } = render(Button, {
      props: { ...buttonData, title: "Button Title" },
    });
    await waitFor(() => {
      const button = getByTestId(testId);
      expect(button).toHaveTextContent("Button Title");
      expect(button).toMatchSnapshot();
    });
  });

  it("primary", async () => {
    expect.hasAssertions();

    const { getByTestId } = render(Button, {
      props: { ...buttonData, type: type.primary },
    });
    await waitFor(() => {
      const button = getByTestId(testId);
      expect(button.classList.contains(type.primary)).toBe(true);
      expect(button).toMatchSnapshot();
    });
  });

  it("secondary", async () => {
    expect.hasAssertions();

    const { getByTestId } = await render(Button, {
      props: { ...buttonData, type: type.secondary },
    });
    await waitFor(() => {
      const button = getByTestId(testId);
      expect(button.classList.contains(type.secondary)).toBe(true);
      expect(button).toMatchSnapshot();
    });
  });

  it("transparent", async () => {
    expect.hasAssertions();

    const { getByTestId } = await render(Button, {
      props: { ...buttonData, type: type.transparent },
    });
    await waitFor(() => {
      const button = getByTestId(testId);
      expect(button.classList.contains(type.transparent)).toBe(true);
      expect(button).toMatchSnapshot();
    });
  });
});
