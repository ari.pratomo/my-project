import { type } from "@constants/button";
import { argData, buttonData } from "./data";
import Button from "./pure";
import { CompGroup } from "@constants/story";

export default {
  title: `${CompGroup}/Button/Basic`,
  component: Button,
  excludeStories: /.*Data$/,
  argTypes: argData,
};

const Template = ({ onClick, ...args }) => ({
  Component: Button,
  props: {
    ...args,
  },
  on: {
    click: onClick,
  },
});

export const Default = Template.bind({});
Default.args = {
  ...buttonData,
  title: "Button",
};

export const Empty = Template.bind({});

export const Normal = Template.bind({});
Normal.args = {
  ...buttonData,
  title: "Button",
};

export const LongText = Template.bind({});
LongText.args = {
  ...buttonData,
  title: "This is very very very long text for mobile viewport",
};

export const Loading = Template.bind({});
Loading.args = {
  ...buttonData,
  title: "Button",
  loading: true,
};

export const Disabled = Template.bind({});
Disabled.args = {
  ...buttonData,
  title: "Button",
  disabled: true,
};

export const Primary = Template.bind({});
Primary.args = {
  ...buttonData,
  title: "Button",
  type: type.primary,
};

export const Secondary = Template.bind({});
Secondary.args = {
  ...buttonData,
  title: "Button",
  type: type.secondary,
};

export const Transparent = Template.bind({});
Transparent.args = {
  ...buttonData,
  title: "Button",
  type: type.transparent,
};
