import styled from 'styled-components'
import { colors } from '@constants/theme'

export const SectionDivider = styled.hr`
  margin-top: 52px;
  margin-bottom: 42px;
  display: block;
  border: 0;
  text-align: center;
  overflow: visible;

  &:before {
    --x-height-multiplier: 0.342;
    --baseline-multiplier: 0.22;
    font-weight: 400;
    font-style: italic;
    font-size: 30px;
    letter-spacing: 0.6em;
    content: '...';
    display: inline-block;
    margin-left: 0.6em;
    color: rgba(0, 0, 0, 0.68);
    position: relative;
    top: -30px;
  }
`

export const SectionPad = styled.div`
  margin-top: 60px;
  &:before {
    content: '';
  }
`

export const DoTitle = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.green50};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 20px;
  color: ${colors.white};
`

export const DoDesc = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.green10};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${colors.black70};
`

export const DoComp = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.white30};
`

export const DontTitle = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.red50};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 20px;
  color: ${colors.white};
`

export const DontDesc = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.red10};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${colors.black70};
`

export const DontComp = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.white30};
`

export const InfoTitle = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.blue40};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 20px;
  color: ${colors.white};
`

export const InfoComp = styled.div`
  display: block;
  padding: 8px 16px;
  background-color: ${colors.white30};
`

export const Li = styled.li`
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
`

export const P = styled.p`
  margin: 4px 0px 10px 0px;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;

  &: first-of-type {
    margin: 4px 0px 10px 0px !important;
  }
`
