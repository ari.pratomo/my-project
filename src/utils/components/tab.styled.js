import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import { Tabs, useTabState } from '@bumaga/tabs'

const cn = (...args) => args.filter(Boolean).join(' ')

export const STab = ({ children }) => {
  const { isActive, onClick } = useTabState()

  return (
    <Tab className={cn('tab', isActive && 'active')} onClick={onClick}>
      {children}
    </Tab>
  )
}

export const Tab = styled.button`
  outline: none;
  cursor: pointer;
  border: none;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  color: #9ca4ac;
  padding: 8px 16px;
  background-color: white;
  transition: color 0.16s ease-in-out, background-color 0.16s ease-in-out,
    border-color 0.16s ease-in-out;

  &.tab.active {
    cursor: default;
    border-bottom: 4px solid #ff2c2c;
    color: #52575c;
  }
`

export const STabs = styled.div`
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-direction: column;
`

export const STabList = styled.div`
  display: flex;
  padding-bottom: 48px;
  padding-top: 40px;
`

export const STabProgress = styled.div`
  position: absolute;
  left: -500px;
  right: -500px;
  top: 80px;
  height: 1px;
  z-index: 0;
  background-color: #e7e7f0;
  transform-origin: 0%;
`

export const SuperTabs = ({ children }) => {
  const [index, setIndex] = useState(0)

  useEffect(() => {
    setIndex(1)
  }, [])

  return <Tabs state={[index, setIndex]}>{children}</Tabs>
}
