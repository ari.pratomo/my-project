export const colors = {
  red10: '#FFDCDC',
  red20: '#FFB7B7',
  red40: '#FF2C2C',
  red30: '#FF7475',
  red50: '#D90102',
  red60: '#750000',
  white: '#FFFFFF',
  white30: '#EFF0F7',
  black10: '#F8F8FC',
  black20: '#E7E7F0',
  black30: '#E1E1ED',
  black40: '#9CA4AC',
  black50: '#52575C',
  black60: '#282D31',
  black70: '#001122',
  blue20: '#C2F3FF',
  blue40: '#00C3F2',
  blue50: '#01A3CC',
  green10: '#F1FAEF',
  green20: '#C7ECC1',
  green30: '#9DDD92',
  green40: '#64CA51',
  green50: '#47AC35',
  green60: '#337D26',
}
