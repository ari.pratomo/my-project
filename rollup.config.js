import svelte from "rollup-plugin-svelte";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import livereload from "rollup-plugin-livereload";
import alias from "@rollup/plugin-alias";
import { terser } from "rollup-plugin-terser";
import cleaner from "rollup-plugin-cleaner";

const pkg = require("./package.json");

const path = require("path");
const projectRootDir = path.resolve(__dirname);
const name = pkg.name
  .replace(/^(@\S+\/)?(svelte-)?(\S+)/, "$3")
  .replace(/^\w/, m => m.toUpperCase())
  .replace(/-\w/g, m => m[1].toUpperCase());

const production = !process.env.ROLLUP_WATCH;
const compnentList = {
  index: "./src/index.js",
  coba: "./src/components/coba/index.js",
  // button: "src/components/button/pure/index.js",
  // "button-bottom": "./src/components/button/bottom/bottom.svelte",
  // accordion: "src/components/accordion/index.js",
};

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require("child_process").spawn(
        "npm",
        ["run", "start", "--", "--dev"],
        {
          stdio: ["ignore", "inherit", "inherit"],
          shell: true,
        }
      );

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}
export default [
  {
    // for monolith publish & run dev svelte
    input: !production ? "src/main.js" : "src/index.js",
    output: [
      { file: pkg.module, format: "es" },
      { file: pkg.main, format: "umd", name },
    ],
    plugins: [
      svelte({
        dev: !production,
        css: css => {
          css.write("bundle.css");
        },
        emitCss: false,
      }),
      cleaner({
        targets: ["public/build/"],
      }),
      nodeResolve({
        browser: true,
        dedupe: ["svelte"],
      }),
      commonjs(),
      alias({
        resolve: [".jsx", ".js", ".svelte", ".json"],
        entries: [
          {
            find: "~",
            replacement: path.resolve(projectRootDir),
          },
          {
            find: "@",
            replacement: path.resolve(projectRootDir, "src"),
          },
          {
            find: "@components",
            replacement: path.resolve(projectRootDir, "src/components"),
          },
          {
            find: "@constants",
            replacement: path.resolve(projectRootDir, "src/constants"),
          },
          {
            find: "@utils",
            replacement: path.resolve(projectRootDir, "src/utils"),
          },
          {
            find: "@stores",
            replacement: path.resolve(projectRootDir, "src/stores"),
          },
        ],
      }),
      // In dev mode, call `npm run start` once
      // the bundle has been generated
      !production && serve(),

      // Watch the `public` directory and refresh the
      // browser on changes when not in production
      !production && livereload("public"),

      // If we're building for production (npm run build
      // instead of npm run dev), minify
      production && terser(),
    ],
    watch: {
      clearScreen: false,
    },
  },
  {
    // for per component publish only
    input: !production ? "src/main.js" : compnentList,
    output: [
      {
        dir: "components",
        format: "esm",
        sourcemap: true,
        exports: "named",
      },
    ],
    plugins: [
      svelte({
        dev: !production,
        css: css => {
          css.write("bundle.css");
        },
        emitCss: false,
        compilerOptions: {
          generate: "ssr",
          hydratable: true,
        },
      }),
      // nodeResolve({
      //   browser: true,
      //   dedupe: ["svelte"],
      // }),
      // commonjs(),
      alias({
        resolve: [".jsx", ".js", ".svelte", ".json"],
        entries: [
          {
            find: "~",
            replacement: path.resolve(projectRootDir),
          },
          {
            find: "@",
            replacement: path.resolve(projectRootDir, "src"),
          },
          {
            find: "@components",
            replacement: path.resolve(projectRootDir, "src/components"),
          },
          {
            find: "@constants",
            replacement: path.resolve(projectRootDir, "src/constants"),
          },
          {
            find: "@utils",
            replacement: path.resolve(projectRootDir, "src/utils"),
          },
          {
            find: "@stores",
            replacement: path.resolve(projectRootDir, "src/stores"),
          },
        ],
      }),
    ],
  },
];
